#ifndef _SEPPL_MODULE_H
#define _SEPPL_MODULE_H

/* $Id$ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <linux/spinlock.h>
#include <linux/crypto.h>

#if 0
#define DEBUGP printk
#else
#define DEBUGP(format, args...)
#endif

#include "seppl_protocol.h"

struct seppl_key {
    u8 algorithm;
    char name[7];
    atomic_t ready;
    atomic_t usage;

    struct crypto_tfm *tfm;
    spinlock_t iv_spinlock;
    u8 *iv;
    u8 *key;

    struct crypto_tfm *tfm_ecb;	/* for encrypting iv */
    u8 *key_ecb;

    unsigned int ivsize;
    unsigned int keysize;
    unsigned int blocksize;

    struct seppl_key *next;
};

struct seppl_key* seppl_claim_key(u8 algorithm, const char *name);
void seppl_release_key(struct seppl_key *key);
void seppl_copy_iv(struct seppl_key *key, u8* iv);

#endif
