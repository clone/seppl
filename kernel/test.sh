#!/bin/sh

# $Id$
#
# This file is part of seppl.
#
# seppl is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# seppl is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with seppl; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

dmesg -n 7

iptables -t mangle -F POSTROUTING
iptables -t mangle -F PREROUTING

sync

make unload
sleep 2
make load

../utils-python/seppl-ls -f /etc/seppl/ring2.keys > /proc/net/seppl_keyring
../utils-python/seppl-ls

iptables -t mangle -A POSTROUTING -p tcp --tcp-flags SYN,RST SYN -d 10.0.0.0/8 -j TCPMSS --set-mss $((1500-40-8-6-16))
iptables -t mangle -A POSTROUTING -d 10.0.0.0/8 -j CRYPT --key `hostname`
iptables -t mangle -A PREROUTING -d 10.0.0.0/8 -j DECRYPT

iptables -t mangle -L 
