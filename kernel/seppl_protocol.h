#ifndef _SEPPL_H
#define _SEPPL_H

/* $Id$ */

/***  
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#define SEPPL_PROTOCOL 177

// Packet layout:
// 1. IP-Header                             PLAIN
// 2. Uncrypted header                      PLAIN
// 3. iv                                    PLAIN
// 4. Crypted header                        CRYPTED
// 5. Payload (with TCP/UDP/...-Header)     CRYPTED
// 6. Padding                               CRYPTED

struct seppl_uncrypt_hdr {
    uint8_t algorithm;
    char key_name[7];
} __attribute__((packed)) uncrypt_hdr;

struct seppl_crypt_hdr{
    uint8_t ident; // 0x00
    uint8_t saved_protocol;
    uint16_t saved_tot_len;
    uint16_t saved_frag_off;
} __attribute__((packed)) crypt_hdr;


#endif
