/* $Id$ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>

#include <iptables.h>
#include <linux/netfilter_ipv4/ip_tables.h>

#include "ipt_CRYPT.h"


/* Function which prints out usage message. */
static void help(void) {
	printf("CRYPT options:\n"
           "  --algorithm algo              Select algorithm\n"
           "  --key key                     Select key\n");
}

static struct option opts[] = {
	{ "algorithm", 1, 0, 'a' },
	{ "key", 1, 0, 'k' },
	{ 0 }
};

static void init(struct ipt_entry_target *t, unsigned int *nfcache) {
	struct ipt_crypt_info *crypt = (struct ipt_crypt_info *) t->data;

    memset(crypt, 0, sizeof(struct ipt_crypt_info));
	strncpy(crypt->name, "def", 7);

    *nfcache |= NFC_UNKNOWN;
}

static int parse(int c, char **argv, int invert, unsigned int *flags, const struct ipt_entry *entry, struct ipt_entry_target **target) {
	struct ipt_crypt_info *crypt = (struct ipt_crypt_info *)(*target)->data;

	switch(c) {
        case 'a':
            crypt->algorithm = atoi(optarg);
            return 1;

        case 'k':
            strncpy(crypt->name, optarg, 7);
            return 1;
            
	}

    return 0;
}

static void final_check(unsigned int flags) {
}

static void print(const struct ipt_ip *ip, const struct ipt_entry_target *target, int numeric) {
	const struct ipt_crypt_info *crypt = (const struct ipt_crypt_info *)target->data;
    char txt[8];

    strncpy(txt, crypt->name, 7);
    txt[7] = 0;
    
    printf("key: %s algo: %i\n", txt, crypt->algorithm);
}

static void save(const struct ipt_ip *ip, const struct ipt_entry_target *target) {
	const struct ipt_crypt_info *crypt = (const struct ipt_crypt_info *)target->data;
    char txt[8];

    strncpy(txt, crypt->name, 7);
    txt[7] = 0;
    
    printf("--algorithm %i ", crypt->algorithm);
    printf("--key '%s'", txt);

}

static struct iptables_target crypt = {
    NULL,
    "CRYPT",
    IPTABLES_VERSION,
    IPT_ALIGN(sizeof(struct ipt_crypt_info)),
    IPT_ALIGN(sizeof(struct ipt_crypt_info)),
    &help,
    &init,
    &parse,
    &final_check,
    &print,
    &save,
    opts
};

void _init(void) {
	register_target(&crypt);
}
