/* $Id$ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
                                                                                                                                                                                                                    
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <iptables.h>
#include <linux/netfilter_ipv4/ip_tables.h>

#include "ipt_DECRYPT.h"


/* Function which prints out usage message. */
static void help(void) {
    printf("DECRYPT options: none");
}

static struct option opts[] = {
    { 0 }
};

static void init(struct ipt_entry_target *t, unsigned int *nfcache) {
    *nfcache |= NFC_UNKNOWN;
}

static int parse(int c, char **argv, int invert, unsigned int *flags, const struct ipt_entry *entry, struct ipt_entry_target **target) {
    return 0;
}

static void final_check(unsigned int flags) {
}

static void print(const struct ipt_ip *ip, const struct ipt_entry_target *target, int numeric) {
}

static void save(const struct ipt_ip *ip, const struct ipt_entry_target *target) {
}

static struct iptables_target decrypt = {
    NULL,
    "DECRYPT",
    IPTABLES_VERSION,
    IPT_ALIGN(sizeof(struct ipt_decrypt_info)),
    IPT_ALIGN(sizeof(struct ipt_decrypt_info)),
    &help,
    &init,
    &parse,
    &final_check,
    &print,
    &save,
    opts
};

void _init(void) {
    register_target(&decrypt);
}
