/* $Id: seppl.c 16 2003-12-04 21:09:48Z lennart $ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>

#include "seppl_common.h"

int genkey(int a, const char *keyname, int as_xml) {
    struct Algs *alg;
    int fd;
    int count, sofar;
    char *key;
    
    if (!(alg = find_alg_by_number(a))) {
        fprintf(stderr, "Algorithm '%i' not found.\n", a);
        return -1;
    }

    if (!(key = (char*) malloc(alg->bits/8))) {
        perror("Failed to allocate memory");
        return -1;
    }

    if ((fd = open( "/dev/random", O_RDONLY)) < 0) {
        perror("Failed to open /dev/random");
        free(key);
        return -1;
    }

    sofar = 0;
    while (sofar < alg->bits/8 ) {
	count = read( fd, &key[sofar], alg->bits/8 - sofar );
	if (count <= 0) {
            fprintf(stderr, "Failed to read key data.\n");
            free(key);
            close(fd);
            return -1;
        }
	sofar += count;
    }

    close(fd);

    if (as_xml) {
	printf("<seppl-keyring>\n");
	dump_key_xml(a, keyname, key);
	printf("</seppl-keyring>\n");
    } else
	dump_key_bin(a, keyname, key);

    free(key);
    
    return 0;
}

static struct option long_options[] = {
    { "algorithm", 1, 0, 'a' },
    { "bits", 1, 0, 'b' },
    { "help", 0, 0, 'h' },
    { "name", 1, 0, 'n' },
    { "no-xml", 0, 0, 'x' },
};

/* default values */
char algorithm[32] = "aes";
char keyname[8] = "def";
int bits = 128;
int asxml = 1;

void usage(void) {
    printf("seppl-gen-key:\n"
           "   -h --help                   Show this help\n"
           "   -x --no-xml                 No xml output\n"
           "   -a --algorithm ALGORITHM    Specify algorithm (%s)\n"
           "   -b --bits BITS              Specify key length (%d)\n"
           "   -n --name NAME              Specify key name (%s)\n", algorithm, bits, keyname);
}

int main(int argc, char *argv[]) {
    int c;
    int option_index = 0;
    int a;

    while((c = getopt_long( argc, argv, "hxa:b:n:", long_options, &option_index )) != -1) {

    	switch( c ) {
	    case 'a':
		/* algorithm */
		strncpy( algorithm, optarg, 31 );
		algorithm[31] = '\0';
		break;
	    case 'b':
		/* bits */
		bits = atoi( optarg );
		break;
	    case 'h':
		/* help */
		usage();
                return 1;
	    case 'n':
		/* name */
		strncpy( keyname, optarg, 7 );
		keyname[7] = '\0';
		break;
	    case 'x':
		/* no-xml */
		asxml = 0;;
		break;
	}
    }

    

    if ((a = find_alg_by_name( algorithm, bits )) < 0) {
	fprintf( stderr, "ERROR: Cipher not available\n" );
	return 2;
    }

    return genkey(a, keyname, asxml) < 0 ? 3 : 0;
}
