/* $Id: seppl.c 16 2003-12-04 21:09:48Z lennart $ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdio.h>
#include <string.h>
#include "seppl_common.h"

#define NUMALGS 2

struct Algs algs[NUMALGS] = {
    { 0, "aes", 128 },
    { 1, "aes", 192 },
};

struct Algs *find_alg_by_number( int a ) {
    if( a < 0 || a >= NUMALGS )
	return NULL;

    return &algs[a];
}

int find_alg_by_name(const char *n, int b) {
    int i;
    
    for (i = 0; i < NUMALGS; i++)
	if (!strcmp(n, algs[i].name) && b == algs[i].bits)
	    return i;

    return -1;
}

int dump_key_xml(int a, const char *keyname, const unsigned char *key) {
    struct Algs *alg;
    int i;
    int keylen;  
    const unsigned char *ch;

    if (!(alg = find_alg_by_number( a ))) {
	fprintf( stderr, "Error: Algorithm not found.\n" );
	return -1;
    }
    
    keylen = alg->bits / 8;
    
    printf("<key>\n"
           "  <name>");
    
    for (ch = keyname; *ch; ch++) { /* escape XML-special chars */
        switch( *ch ) {
            case '&':
                printf( "&amp;" );
                break;
            case '<':
                printf( "&lt;" );
                break;
            case '>':
                printf( "&gt;" );
                break;
            default:
                putchar(*ch);
                break;
        }
    }
    
    printf("</name>\n"
           "  <algorithm>%s</name>\n"
           "  <bits>%u</bits>\n"
           "  <data>", alg->name, alg->bits);
    
    for (i = 0; i < keylen-1; i++)
        printf("%02x:", key[i]);
    
    printf("%02x"
           "</data>\n"
           "</key>\n", key[keylen-1]);

    return 0;
}

int dump_key_bin (int a, const char *keyname, const unsigned char *key) {
    struct Algs *alg;
    int i;
    int keylen;
    
    if (!(alg = find_alg_by_number(a))) {
	fprintf( stderr, "Error: Algorithm not found.\n" );
	return -1;
    }

    keylen = alg->bits / 8;

    /* algorithm number */
    putchar(a);

    /* key name padded with '\0' characters */
    printf("%s", keyname);
    for (i = strlen(keyname); i < 7; i++ )
        putchar(0);

    /* key data */
    for( i = 0; i < keylen; i++ )
        putchar(key[i]);

    return 0;
}
