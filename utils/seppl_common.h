#ifndef foosepplcommonhfoo
#define foosepplcommonhfoo

/* $Id: seppl.c 16 2003-12-04 21:09:48Z lennart $ */

/***
  This file is part of seppl

  seppl is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  seppl is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with seppl; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

struct Algs {
    int i;
    char *name;
    int bits;
};

struct Algs *find_alg_by_number(int a);
int find_alg_by_name(const char *n, int b);
int dump_key_xml(int a, const char *keyname, const unsigned char *key);
int dump_key_bin(int a, const char *keyname, const unsigned char *key);

#endif
