#!/usr/bin/perl

# $Id$
#
# This file is part of seppl.
#
# seppl is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# seppl is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with seppl; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

from struct import *
import re, string, sys

algs = { 0 : ('aes', 128), 1 : ('aes', 192)  }

def find_alg_by_number(a):
	global algs

	if algs.has_key(a):
		return algs[a];
	else:
		return ('unknown', 0)

def find_alg_by_name(n, b):
	global algs

	for i in algs.keys():
		if algs[i][0] == n and algs[i][1] == b:
			return i

	return -1

def strhex(s):
	r = ""
	for i in range(len(s)):
		(b,) = unpack("B", s[i])
		r = "%s:%02x" % (r, b)

	return r[1:]

def dump_key_xml(a, name, key):
	alg, bits = find_alg_by_number(a)

	if bits == 0:
		sys.stderr.write("ERROR: Algorithm not found\n")
		return -1
	
	print "<key>"
	print "  <name>%s</name>" % name
	print "  <algorithm>%s</algorithm>" % alg
	print "  <bits>%u</bits>" % bits
	print "  <data>%s</data>" % strhex(key)
	print "</key>"

	return 0

def dump_key_bin(a, name, key):
	alg, bits = find_alg_by_number(a)

	if bits == 0:
		sys.stderr.write("ERROR: Algorithm not found\n")
		return -1

	if len(key) != bits/8:
		sys.stderr.write("ERROR: Key has wrong size\n")
		return -1

	name = name.encode("iso8859-1", 'ignore')

	sys.stdout.write(pack("B7s", a, name[:7]))
	sys.stdout.write(key)
	sys.stdout.flush()

def parse_key(data):

	k = ""

	r = re.compile("^([0-9A-Fa-f][0-9A-Fa-f])")

	while len(data) > 0:
		m = r.match(data+":")

		if m == None:
			return None

		k += pack("B", string.atoi(m.group(0), 16))

		data = data[3:]

	return k

	
		

		
