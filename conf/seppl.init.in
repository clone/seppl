#!/bin/bash

# $Id$
#
# This file is part of seppl.
#
# seppl is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# seppl is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with seppl; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

PROCFILE="/proc/net/seppl_keyring"
SEPPLLS="@sbindir@/seppl-ls"
KEYDIR="@sysconfdir@/seppl"

test -d "$KEYDIR" || exit 1
test -x "$SEPPLLS" || exit 1

load_keys() {
    echo -n "Configuring SEPPL keys: "
    for K in "$KEYDIR"/* ; do 
        if test -f "$K" ; then
            if ( "$SEPPLLS" -f $K > "$PROCFILE" ) 2> /dev/null ; then
                echo -n "$K "
            else
                echo "failed"
                exit 1
            fi
        fi
    done
    echo "."
}

clear_keys() {
    echo -n "Clearing SEPPL keys: "
    ( echo "clear" > "$PROCFILE" ) 2> /dev/null

    local FAIL=1

    for (( i=0; $i < 5; i++)); do 
        if ! test -s "$PROCFILE" ; then 
            FAIL=0
            break
        fi
        sleep 1
    done

    if [ "$FAIL" -ne 0 ]; then
        echo "failure"
        exit 1
    fi

    echo "success"
}


load_modules() {
    echo -n "Loading SEPPL module: "
    modprobe seppl &> /dev/null
    local FAIL=1
    for (( i=0; $i < 5; i++)); do 
        if test -f "$PROCFILE" ; then
            FAIL=0
            break
        fi

        sleep 1
    done

    if [ "$FAIL" -ne 0 ] ; then
        echo "failure"
        exit 1
    fi

    echo "success"
}

unload_modules() {
    echo -n "Unloading SEPPL modules: "
    modprobe -r ipt_CRYPT &> /dev/null
    modprobe -r ipt_DECRYPT &> /dev/null
    modprobe -r seppl &> /dev/null

    local FAIL=1

    for (( i=0; $i < 5; i++)); do 
        if ! test -f "$PROCFILE" ; then
            FAIL=0
            break
        fi
        sleep 1
    done

    if [ "$FAIL" -ne 0 ] ; then
        echo "failure"
        exit 1
    fi

    echo "success"
}

case "$1" in
    start)
        load_modules
        load_keys
        ;;

    stop)
        clear_keys
        unload_modules
        ;;

    force-reload|reload)
        clear_keys
        load_keys
        ;;

    restart)
        $0 stop
        sleep 1
        $0 start
        ;;

    *)  
    	echo "Usage: $0 {start|stop|restart|force-reload|reload}"
        exit 1
        ;;
esac

exit 0
